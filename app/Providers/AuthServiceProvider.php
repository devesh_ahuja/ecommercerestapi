<?php

namespace App\Providers;

use App\Models\Buyer;
use App\Models\Seller;
use App\Models\Transaction;
use App\Policies\BuyerPolicy;
use App\Policies\SellerPolicy;
use App\Policies\TransactionPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        Buyer::class => BuyerPolicy::class,
        Seller::class => SellerPolicy::class,
        Transaction::class => TransactionPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::tokensExpireIn(now()->addMinutes(15));
        Passport::refreshTokensExpireIn(now()->addMinutes(30));

        Passport::tokensCan([
            'purchase-product' => 'Create a new transaction for a specific product',
            'manage-account' => 'Read your account data such as id, name, email, is_verified and is_admin (password is not given). Modify your account data (email and password). CANNOT DELETE ACCOUNT ',
            'manage-product' => 'CRUD for products',
            'read-general' => 'Read general info like cateogories, products, purchased products, your transactions'
        ]);
    }
}
